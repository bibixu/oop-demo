<?php
    /*PTB2: Phương trình bậc 2
    khai báo các thuộc tính cấu thành PTB2 : ax2 + bx +c =0 & set value cho thuộc tính.
    */
     class PTB2{

         protected $a = 1;
         protected $b = 5;
         protected $c = 4;
         //function title : hiển thị tiêu đề của chương trình
         public function title()
         {
             echo "Kết quả PTB2 : </br>";
         }
    }
    /* Giai_PTB2 : Giải phương trình bậc 2
    Description: Class xử lý giảo phương trình bậc 2.
    output: xuất ra màn hình nghiệm x1,x2
    */
    class Giai_PTB2 extends PTB2{
        
        public function NT()
        {
            parent::NT();//  class Demo overwire function của class OPP ( tính đa hình)
            $a1 =   $this->a;
            $b1 =   $this->b;
            $c1 =   $this->c;
  
            $delta = ($b1 * $b1) - (4 * $a1 * $c1);
         
            if ($delta < 0){
                echo 'Phương trình vô nghiệm';
            }
            else if ($delta == 0){
                echo 'Phương trình nghiệp kép x1 = x2 = ' . (-$b1/2*$a1);
            }
            else {
                echo'Phương trình có hai nghiệp phân biệt, x1 = ' . ((-$b1 + sqrt($delta))/ 2 * $a1);
                echo ',x2 = ' . ((-$b1 - sqrt($delta))/2 * $a1);
            }
           
            
        }
    }
    // class truy_tuong: trừu tượng
    //khai báo function loi_cam_on (lời cảm ơn)
    abstract class truu_tuong
        {
            abstract public function loi_cam_on();
        }
    //class TT_truu_tuong : thực thi trừu tượng
    //function loi_cam_on kế thừa từ class truu_tuong
    // out: Xuất ra màn hình text
    class TT_truu_tuong extends truu_tuong
    {
        public function loi_cam_on()
        {
            echo "XIN CẢM ƠN </br>";
        }
    }
    $hienthi    =   new TT();
    $hienthi->loicamon();
   
    
        $show = new Demo();
        $show->NT();
        

 
    
   
?> 
